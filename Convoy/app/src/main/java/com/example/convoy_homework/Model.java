package com.example.convoy_homework;

import com.google.gson.annotations.SerializedName;

public class Model {

    private String country,city;
    @SerializedName("name")
    private String na;

    @SerializedName("imgURL")
    private String imgUrl;

    public String getName() {
        return na;
    }

    public void setName(String name) {
        this.na = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}
