package com.example.convoy_homework;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RestApiInterface {

    String BASEURL = "https://demonuts.com/Demonuts/JsonTest/Tennis/";

    @GET("json_parsing.php")
    Call<String> getString();

}
