package com.example.convoy_homework;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {


    private LayoutInflater inflater;
    private ArrayList<Model> dataModelArrayList;


    public RecyclerViewAdapter(Context context, ArrayList<Model> dataModelArrayList ){
        inflater = LayoutInflater.from(context);
        this.dataModelArrayList = dataModelArrayList;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.retrofit_item, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }



    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Picasso.get().load(dataModelArrayList.get(position).getImgUrl()).into(holder.iv);
        holder.name.setText(dataModelArrayList.get(position).getName());
        holder.country.setText(dataModelArrayList.get(position).getCountry());
        holder.city.setText(dataModelArrayList.get(position).getCity());

    }


    @Override
    public int getItemCount() {
        return dataModelArrayList.size();
    }



    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView iv;
        TextView name,country,city;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name);
            country = itemView.findViewById(R.id.country);
            city = itemView.findViewById(R.id.city);
            iv = itemView.findViewById(R.id.image_view);

        }
    }
}
